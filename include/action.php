<?php 
    
    include 'classes.php';
    session_start();
    
    

    $obj = new DataOperation;

    
    
    if(isset($_POST['delete'])) 
    {
        $check = $_POST['checkbox'];
        
        foreach($check as $id)
        {
            $where = array("id" => $id);
            $obj->delete_product("product_info", $where);
        }

        $_SESSION['message'] = "Selected products have been deleted!";
        

        header('location: ../products.php');
    }

?>