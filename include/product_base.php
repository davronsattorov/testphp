<?php  

    include 'db.php';

    abstract class ProductBase extends Database
    {
        protected $sku = "";
        protected $name = "";
        protected $price = "";

        abstract protected function addProduct($table);
        abstract protected function getProduct($table);
        
    }

    class ProductDvd extends ProductBase
    {

        public function addProduct($table)
        {

            $info = array (
                'sku' => $_POST['sku'],
                'name' => $_POST['name'],
                'price' => $_POST['price'],
                'prod' => $_POST['prod'],
                'size' => $_POST['size']
            );

            $sql = "";
            $sql .= "INSERT INTO ".$table;
            $sql .= " (".implode(",", array_keys($info)).") VALUES ";
            $sql .= "('".implode("','", array_values($info))."')";
            $query = mysqli_query($this->connection, $sql);
            if($query)
            {
                return true;
            }
        }

        public function getProduct($table) 
        {
            $sql = "SELECT * FROM " .$table. " WHERE prod = 'dvd'";
            $query = mysqli_query($this->connection, $sql);
            $array = array();
            while($row = mysqli_fetch_assoc($query))
            {
                $array[] = $row;
            }

            return $array;
            
        }
    }

    class ProductBook extends ProductBase
    {

        public function addProduct($table)
        {

            $info = array (
                'sku' => $_POST['sku'],
                'name' => $_POST['name'],
                'price' => $_POST['price'],
                'prod' => $_POST['prod'],
                'weight' => $_POST['weight']
            );

            $sql = "";
            $sql .= "INSERT INTO ".$table;
            $sql .= " (".implode(",", array_keys($info)).") VALUES ";
            $sql .= "('".implode("','", array_values($info))."')";
            $query = mysqli_query($this->connection, $sql);
            if($query)
            {
                return true;
            }
        }

        public function getProduct($table) 
        {
            $sql = "SELECT * FROM " .$table. " WHERE prod = 'book'";
            $query = mysqli_query($this->connection, $sql);
            $array = array();
            while($row = mysqli_fetch_assoc($query))
            {
                $array[] = $row;
            }

            return $array;
            
        }
    }

    class ProductFurniture extends ProductBase
    {

        public function addProduct($table)
        {

            $info = array (
                'sku' => $_POST['sku'],
                'name' => $_POST['name'],
                'price' => $_POST['price'],
                'prod' => $_POST['prod'],
                'height' => $_POST['height'],
                'width' => $_POST['width'],
                'length' => $_POST['length']
            );

            $sql = "";
            $sql .= "INSERT INTO ".$table;
            $sql .= " (".implode(",", array_keys($info)).") VALUES ";
            $sql .= "('".implode("','", array_values($info))."')";
            $query = mysqli_query($this->connection, $sql);
            if($query)
            {
                return true;
            }
        }

        public function getProduct($table) 
        {
            $sql = "SELECT * FROM " .$table. " WHERE prod = 'furniture'";
            $query = mysqli_query($this->connection, $sql);
            $array = array();
            while($row = mysqli_fetch_assoc($query))
            {
                $array[] = $row;
            }

            return $array;
            
        }
    }
?>