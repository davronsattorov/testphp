    $(document).ready(function(){

        $("#dvd-size-1").hide();
        $("#dvd-size-2").hide();
        $("#weight-1").hide();
        $("#weight-2").hide();
        $("#height-1").hide();
        $("#height-2").hide();
        $("#width-1").hide();
        $("#width-2").hide();
        $("#length-1").hide();
        $("#length-2").hide();

      $("#customRadio").click(function(){
        $("#dvd-size-1").show();
        $("#dvd-size-2").show();
        $("#weight-1").hide();
        $("#weight-2").hide();
        $("#height-1").hide();
        $("#height-2").hide();
        $("#width-1").hide();
        $("#width-2").hide();
        $("#length-1").hide();
        $("#length-2").hide();
      });

      $("#customRadio2").click(function(){
        $("#dvd-size-1").hide();
        $("#dvd-size-2").hide();
        $("#weight-1").show();
        $("#weight-2").show();
        $("#height-1").hide();
        $("#height-2").hide();
        $("#width-1").hide();
        $("#width-2").hide();
        $("#length-1").hide();
        $("#length-2").hide();
      });

      $("#customRadio3").click(function(){
        $("#dvd-size-1").hide();
        $("#dvd-size-2").hide();
        $("#weight-1").hide();
        $("#weight-2").hide();
        $("#height-1").show();
        $("#height-2").show();
        $("#width-1").show();
        $("#width-2").show();
        $("#length-1").show();
        $("#length-2").show();
      });

    });