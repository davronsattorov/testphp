error_reporting(-1);
<?php include("include/header.php")?>
<?php include("include/product_base.php")?>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script>
      $(document).ready(function () {
          $('.custom-control-input').on('click', function () {
              $(this).closest('form').find(':checkbox').prop('checked', this.checked);
          });
      });
  </script>
  <br>
   
    <?php if(isset($_SESSION['message'])): ?>

        <div class="alert alert-danger">
            <?php 
            echo $_SESSION['message'];
            echo "<br>";
            unset($_SESSION['message']);
            ?>
            
        </div>

    <?php endif ?>

  
  <form action="include/action.php" method="post">
    <div class="custom-control custom-radio custom-control-inline" style="margin-left: 10rem;">
        <input type="checkbox" class="custom-control-input" id="check_all">
        <label class="custom-control-label" for="check_all">Select all products</label>
    </div> 
  <input type="submit" name="delete" id="delete" value="Delete selected products">
  
  <br>
  <br>
  <br>

  <div class="container">
      <div class="row justify-content-center">
          <?php 
            $dvd = new ProductDvd;
            $book = new ProductBook;
            $furniture = new ProductFurniture;
            
            $array_of_dvds = $dvd->getProduct('product_info');
            $array_of_books = $book->getProduct('product_info');
            $array_of_furniture = $furniture->getProduct('product_info');
           ?>

           <?php 

            if(!empty($array_of_dvds)) // if we have some data about DVD in DB -> display it
            
              foreach ($array_of_dvds as $line)
              {
            ?>
                <div class='col-md-4'>
                  <div class='card text-center'>
                      <div class='card-header'>
                          <input type='checkbox' name = 'checkbox[]' value = '<?=$line['id']?>' style='float: left;'>
                          <h5 class='card-subtitle mb-2 text-muted'>SKU: <?=strtoupper($line['sku'])?></h5>
                  </div>
                  <div class='card-body'>                        
                      <h6 class='card-title'>Name: <?=ucfirst($line['name'])?></h6><br>
                      <h6 class='card-subtitle mb-2'>Price: $<?=$line['price']?></h6><br>
                      <h6 class='card-subtitle mb-2'>Size: <?=$line['size']?> MB</h6><br>
                  </div>
                  </div><br>
                  </div>

              <?php
              }
              ?>



            <?php 

            if(!empty($array_of_books)) // if we have some data about Books in DB -> display it
            
              foreach ($array_of_books as $line)
              {
            ?>
                <div class='col-md-4'>
                  <div class='card text-center'>
                      <div class='card-header'>
                          <input type='checkbox' name = 'checkbox[]' value = '<?=$line['id']?>' style='float: left;'>
                          <h5 class='card-subtitle mb-2 text-muted'>SKU: <?=strtoupper($line['sku'])?></h5>
                  </div>
                  <div class='card-body'>                        
                      <h6 class='card-title'>Name: <?=ucfirst($line['name'])?></h6><br>
                      <h6 class='card-subtitle mb-2'>Price: $<?=$line['price']?></h6><br>
                       <h6 class='card-subtitle mb-2'>Weight: <?=$line['weight']?> KG</h6><br>
                  </div>
                  </div><br>
                  </div>

              <?php
              }
              ?>



              <?php 

            if(!empty($array_of_furniture)) // if we have some data about Furniture in DB -> display it
            
            foreach ($array_of_furniture as $line)
            {
                ?>
                <div class='col-md-4'>
                  <div class='card text-center'>
                      <div class='card-header'>
                          <input type='checkbox' name = 'checkbox[]' value = '<?=$line['id']?>' style='float: left;'>
                          <h5 class='card-subtitle mb-2 text-muted'>SKU: <?=strtoupper($line['sku'])?></h5>
                      </div>
                      <div class='card-body'>                        
                          <h6 class='card-title'>Name: <?=ucfirst($line['name'])?></h6><br>
                          <h6 class='card-subtitle mb-2'>Price: $<?=$line['price']?></h6><br>
                       <h6 class='card-subtitle mb-2'>Dimensions: <?=$line['height']?> x <?=$line['width']?> x <?=$line['length']?>(HxWxL)</h6><br>
                      </div>
                  </div><br>
              </div>

              <?php
          }
          ?>






      </div>
  </div>
  </form>
  <br>
  <br>
  <br>
  


<?php include("include/footer.php") ?>