error_reporting(-1);
<?php include("include/header.php")?>
<?php include("include/product_base.php")?>

<br>
    
<?php if(isset($_SESSION['message'])): ?>

<div class="alert alert-success">
  <?php 
    echo $_SESSION['message'];
    unset($_SESSION['message']);
  ?>
</div>

<?php endif ?>

<?php 

if(isset($_POST['save'])) 
{
    switch($_POST['prod'])
    {
      case 'dvd': $dvd = new ProductDvd;
                  $dvd->addProduct('product_info');
                  break;
      case 'book': $book = new ProductBook;
                  $book->addProduct('product_info');
                  break;
      case 'furniture': $furniture = new ProductFurniture;
                        $furniture->addProduct('product_info');
                        break;        
    }

    
    $_SESSION['message'] = "Product has been added successfully!";
    header('location: index.php?msg=Product added');
    
}

?>

    <form method="post" action="">
      <div class="container-1">
      <div class="row">
        <div class="col-50">
          <h3>Add a Product</h3>

          <label for="sku">SKU</label>
          <input type="text" id="sku" name="sku" placeholder="Enter SKU">

          <label for="name">Name</label>
          <input type="text" id="name" name="name" placeholder="Enter Name">

          <label for="price">Price</label>
          <input type="text" id="price" name="price" placeholder="Enter Price">

          <label for="prod">Choose product type</label>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="customRadio" name="prod" value="dvd">
            <label class="custom-control-label" for="customRadio">DVD-disk</label>
          </div>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="customRadio2" name="prod" value="book">
            <label class="custom-control-label" for="customRadio2">Book</label>
          </div> 
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="customRadio3" name="prod" value="furniture">
            <label class="custom-control-label" for="customRadio3">Furniture</label>
          </div> 
        </div>

        <div class="col-50">
          <h3>Additional informations</h3>
          
          <label for="dvd-size" id="dvd-size-1">Size</label>
          <input type="text" id="dvd-size-2" name="size" placeholder="Enter size in MB">

          <label for="weight" id="weight-1">Weight</label>
          <input type="text" id="weight-2" name="weight" placeholder="Enter weight in KG">

          <label for="height" id="height-1">Height</label>
          <input type="number" id="height-2" name="height" placeholder="Enter height in CM">

          <label for="width" id="width-1">Width</label>
          <input type="number" id="width-2" name="width" placeholder="Enter width in CM">
          
          <label for="length" id="length-1">Length</label>
          <input type="number" id="length-2" name="length" placeholder="Enter width in CM">
          
        </div>
        </div>
      </div>
      <input type="submit" name="save" value="Add Product" class="btn">
    </form>
    

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="/js/main.js"></script>
<?php include("include/footer.php") ?>