<?php 

include "db.php";


class DataOperation extends Database
    {

        public function delete_product($table, $where)
        {
            $sql = "";
            $condition = "";

            foreach ($where as $key => $value)
            {
                $condition .= $key . "='" . $value . "' AND ";
            }
            $condition = substr($condition, 0, -5);
            $sql = "DELETE FROM " .$table. " WHERE " .$condition;

            if(mysqli_query($this->connection, $sql))
            {
                return true;
            }
        }
    }

?>