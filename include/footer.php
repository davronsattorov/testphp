<br>
<br>

<footer class="footer" style="text-align: center;">
    <div class="container">
      <span class="text-muted">&copy; Scandiweb | by Davron Sattorov
      </span>
    </div>
  </footer>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>